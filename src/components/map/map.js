import * as d3 from 'd3';
import { geoMercator as mercator, geoPath, geoCentroid } from 'd3-geo';
import $ from 'jquery';
require('./map.scss');
require('../../services/MapService.js');

angular.module('nextbusApp').directive('map', ["mapService", function(mapService) {
  return {
    restrict: 'E',
    scope: {
      routes: '=',
      selectedRoutes: '='
    },
    controller: function($scope) {
      let vm = this;
      vm.isRefreshing = false;

      d3.json('content/streets.json', function(json) {
        let width  = 800;
        let height = 800;
        var vis = d3.select("#map").append("svg")
            .attr("width", width).attr("height", height);

        // create a first guess for the projection
        var center = geoCentroid(json);
        var scale  = 5000;
        var offset = [width/2, height/2];
        var projection = mercator().scale(scale).center(center)
            .translate(offset);

        // create the path
        var path = geoPath().projection(projection);

        // using the path determine the bounds of the current map and use
        // these to determine better values for the scale and translation
        var bounds  = path.bounds(json);
        var hscale  = scale*width  / (bounds[1][0] - bounds[0][0]);
        var vscale  = scale*height / (bounds[1][1] - bounds[0][1]);
        var scale   = (hscale < vscale) ? hscale : vscale;
        var offset  = [width - (bounds[0][0] + bounds[1][0])/2,
                          height - (bounds[0][1] + bounds[1][1])/2];

        // new projection
        projection = mercator().center(center)
          .scale(scale).translate(offset);
        path = path.projection(projection);

        // add a rectangle to see the bound of the svg
        vis.append("rect").attr('width', width).attr('height', height)
          .style('stroke', 'black').style('fill', 'none');

        vis.selectAll("path").data(json.features).enter().append("path")
          .attr("d", path)
          .style("fill", "yellow")
          .style("stroke-width", "0.5")
          .style("stroke", "black");

        // function to create chars from int
        let intToChar = function(i) {
          return (i >= 26 ? intToChar((i / 26 >> 0) - 1) : '') +
            'abcdefghijklmnopqrstuvwxyz'[i % 26 >> 0];
        };

        let queryVehicleLocations = (routeTag) => {
          // remove all children
          vis.selectAll("circle").remove();
          vis.selectAll("text").remove();

          // get vehicle locations
          return mapService.getVehicleLocations(routeTag.tag);
        };

        let handleRouteResponse = (result) => {
          let xmlResponse = $.parseXML(result.data);
          let xml = $(xmlResponse);
          let vehicles = xml.find('vehicle').toArray();
          let routeTag = (vehicles.length > 0) ?
            $(vehicles[0]).attr('routeTag')
            : null;

          let vehicleCoordinates = vehicles.reduce(function(previous, current) {
            previous.push([$(current).attr('lon'), $(current).attr('lat')]);
            return previous;
          }, []);

          let routeCode = (isNaN(parseInt(routeTag))) ? routeTag : intToChar(routeTag);

          if(vehicleCoordinates.length > 1) {
            let circles = vis.selectAll(`${routeCode}.circle`).data(vehicleCoordinates);
            let labels = vis.selectAll(`${routeCode}.text`).data(vehicleCoordinates);

            // add vehicles to svg
            circles.enter()
              .append("circle")
              .attr("cx", function(d) { return projection(d)[0]; })
              .attr("cy", function(d) { return projection(d)[1]; })
              .attr("r", "4px")
              .attr("fill", "red");

            // add labels
            labels.enter()
              .append("text")
              .attr("x", function(d) { return projection(d)[0]; })
              .attr("y", function(d) { return projection(d)[1]; })
              .text( function(d) { return routeTag; })
              .attr("font-family", "sans-serif")
              .attr("font-size", "18px")
              .attr("fill", "black");
          }
        };

        let drawMap = () => {
          // show loading gif
          vm.isRefreshing = true;

          // filter routes
          let routesToDraw = vm.routes.filter(function(route) {
            // only handle ticked routes
            if (!vm.selectedRoutes[route.tag]) {
              return false;
            }
            return true;
          });

          // get routes
          if(routesToDraw.length > 0) {
            Promise.all(routesToDraw.map((routeTag) => queryVehicleLocations(routeTag))).then(
              (data) => Promise.all(data.map(handleRouteResponse))
            ).then(function() {
              // hide loading gif
              $scope.$apply(function() {
                vm.isRefreshing = false;
              });
            });
          }
        };

        // to let the selected-routes object populate before drawing, a bit of a hack :(
        setTimeout(drawMap, 1000);

        // query vehicle locations every 15 seconds
        setInterval(drawMap, 15000);
      });

    },
    controllerAs: 'vm',
    bindToController: true,
    template: require('./map.html')
  };
}]);
