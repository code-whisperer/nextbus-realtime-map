import $ from 'jquery';
require('../../services/MapService.js');

angular.module('nextbusApp').directive('routes', ["mapService", function(mapService) {
  return {
    restrict: 'E',
    scope: {
    },
    controller: function($scope) {
      let vm = this;
      vm.routes = [];

      // add routes N and 6 as test data
      vm.selectedRoute = {
        active: {
          'N': 'N',
          '6': '6'
        }
      };

      // get routes
      mapService.getRoutes().success(function(result) {
        let xmlResponse = $.parseXML(result);
        let xml = $(xmlResponse);
        let routes = xml.find('route').toArray();

        routes.forEach(function(route) {
          let routeTag = $(route).attr('tag');
          if(routeTag) {
            vm.routes.push({
              tag: routeTag
            });
          }
        });

      });
    },
    controllerAs: 'vm',
    bindToController: true,
    template: require('./routes.html')
  };
}]);
