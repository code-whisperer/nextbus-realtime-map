angular.module('nextbusApp').factory('mapService', ['$http', function($http) {
  return {

    getVehicleLocations: function(routeTag) {
      return $http({
        method: 'GET',
        url: `http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&r=${routeTag}&t=0`
      });
    },
    getRoutes: function() {
      return $http({
        method: 'GET',
        url: 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=sf-muni'
      });
    }
  };
}]);
