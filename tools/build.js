require("babel-polyfill");
const clean = require('./clean.js').default;

function format(time) {
  return time.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');
}

const start = new Date();

function run(taskName, name) {
  const task = require(`./${taskName}.js`).default;

  console.log(`[${format(start)}] Starting ${name}...`);
  task().then(() => {
    const end = new Date();
    const time = end.getTime() - start.getTime();
    console.log(`[${format(end)}] Finished ${name} after ${time} ms`);
  });
}

async function build() {
  await run('copy', 'copying files');
  await run('bundle', 'bundling');
}

clean().then(() => {
  console.log('cleaning up...');
  build().then(() => {
    console.log('building...');
  });
});
