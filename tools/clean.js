import del from 'del';
import mkdirp from 'mkdirp';

/**
 * Cleans up the output (build) directory.
 */
async function clean() {
  await del(['build/*'], { dot: true });
  await makeDir('build');
}

const makeDir = (name) => new Promise((resolve, reject) => {
  mkdirp(name, err => err ? reject(err) : resolve());
});

export default clean;
