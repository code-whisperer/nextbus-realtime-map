import Promise from 'bluebird';

async function copy({ watch } = {}) {
  const ncp = Promise.promisify(require('ncp'));

  await Promise.all([
    ncp('src/content', 'build/content'),
    ncp('index.html', 'build/index.html')
  ]);
}

export default copy;
