var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: "./src/main.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        include: [
          path.resolve(__dirname, './src')
        ],
        query: {
          babelrc: false,
          presets: [
            'es2015',
            'stage-0'
          ],
          plugins: [
            'transform-runtime'
          ]
        }
      },
      {
        test: /\.css$/,
        loader: "style!css"
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract([
          'css?minimize&modules&localIdentName=[local]&importLoaders=2',
          'postcss?parser=postcss-scss',
          'sass'
        ])
      },
      {
        test: /\.html/,
        loader: 'html?name=[name].[ext]'
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10000',
        query: {
          name: '[path][name].[ext]?[hash]',
          limit: 10000
        }
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.(eot|ttf|svg)$/,
        loader: 'file-loader',
        query: {
          name: '[path][name].[ext]?[hash]'
        }
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('styles.css', {
      allChunks: true
    })
  ]
};
